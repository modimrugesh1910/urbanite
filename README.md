# Clarivate Analytics Challenge - URBANITE

![Alt text](src/assets/img/appPrintScreen.png?raw=true "App Printscreen")

The forecast data is provided from openweathermap apis. 

The app is responsive.

Main features why I created this app:

- Angular 2+
- Routing
- Components
- Search
- Get data from an API

## DEMO
https://urbanite.netlify.com/

## Installation

Assuming you have node, npm and git installed:

1. In Git Bash:
    - $ git clone https://modimrugesh1910@bitbucket.org/modimrugesh1910/urbanite.git
    - $ cd URBANITE
    - $ npm install

2. Start application by typing npm start

3. Open http://localhost:4200/ in your browser.
