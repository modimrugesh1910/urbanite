
import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent {
  title = 'URBANITE';

  private apiUrl = 'https://api.openweathermap.org/data/2.5/group?id=1273294,1275339,1277333,1275004,1269843&units=metric&appid=110ff02ed24ccd819801248373c3b208';
  data: any = {};

  constructor(private http: HttpClient) {
    this.getTest();
    this.getData();
  }

  getData() {
    return this.http.get(this.apiUrl);
  }

  getTest() {
    this.getData().subscribe(data => {
      console.log(data);
      this.data = data;
    });
  }
}

